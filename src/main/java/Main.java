import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Sets.newLinkedHashSet;

public class Main {

    /**
     * Compare map with ignoring given entities
     */
    public static void compareMapsWithoutGivenFields(
            Map<String, Object> actual,
            Map<String, Object> expected,
            String... ignoredFieldsList
    ) {
        Set<String> ignoredFields = newLinkedHashSet(ignoredFieldsList);
        if (ignoredFields.isEmpty()) {
            assertThat(actual).containsAllEntriesOf(expected);
        } else {
            compareMapsWithoutFields(actual, expected, ignoredFields);
        }
    }

    /**
     * Compare maps without ignoring entities. The sub objects will be checked for full compliance
     */
    private static void compareMapsWithoutIgnoringSubFields(
            Map<String, Object> actual,
            Map<String, Object> expected,
            Set<String> ignoredFields
    ) {

        ignoredFields.forEach(
                key -> {
                    actual.remove(key);
                    expected.remove(key);
                }
        );
        System.out.println(String.format("Comparing fields : %s ", actual));
        assertThat(actual).containsAllEntriesOf(expected);
    }

    /**
     * Create mapping given ignored fields to the corresponding sub object.
     */
    private static Map<String, Set<String>> createMapSubTypesWithIgnoredSubFields(Set<String> ignoredList) {
        String ignoredSubField = null;
        Map<String, Set<String>> mapSubTypes = new HashMap<>();
        String objectKey = "";
        for (String propertyName : ignoredList) {
            int delimiterIndex = propertyName.indexOf('.');
            if (delimiterIndex != -1) {
                objectKey = propertyName.substring(0, delimiterIndex);
                ignoredSubField = propertyName.substring(delimiterIndex + 1);
            }
            if (ignoredSubField != null && !ignoredSubField.isEmpty()) {
                Set<String> ignoredSubProperties = new HashSet<>(Arrays.asList(ignoredSubField));
                if (mapSubTypes.containsKey(objectKey)) {
                    (mapSubTypes.get(objectKey)).addAll(ignoredSubProperties);
                } else {
                    mapSubTypes.put(objectKey, ignoredSubProperties);
                }
            }
        }
        return mapSubTypes;
    }

    /**
     * Create list of fields which is not a simple java type and which should be ignored in the process of comparison
     */
    private static Set<String> createIgnoredSubTypesFields(Set<String> ignoredList) {
        Set<String> ignoredFields = new HashSet<>();
        for (String propertyName : ignoredList) {
            if (propertyName.indexOf('.') == -1) {
                continue;
            }
            ignoredFields.add(propertyName.substring(0, propertyName.indexOf('.')));
        }
        return ignoredFields;
    }

    /**
     * Compare map with ignoring given entities including sub objects
     */
    private static void compareMapsWithoutFields(
            Map<String, Object> actual,
            Map<String, Object> expected,
            Set<String> ignoredFields
    ) {
        Set<String> subTypesOfIgnoredProperties = createIgnoredSubTypesFields(ignoredFields);
        if (subTypesOfIgnoredProperties.isEmpty()) {
            compareMapsWithoutIgnoringSubFields(actual, expected, ignoredFields);
        } else {
            Map<String, Set<String>> fieldsToSubTypes = createMapSubTypesWithIgnoredSubFields(ignoredFields);
            expected.forEach(
                    (key, value) -> {
                        Object actualObject = actual.get(key);
                        Object expectedObject = expected.get(key);
                        Map<String, Object> actualSubObject = null;
                        Map<String, Object> expectedSubObject = null;
                        if (!subTypesOfIgnoredProperties.contains(key)) {
                            if (!ignoredFields.contains(key)) {
                                System.out.println(String.format("Comparing fields : %s ", key));
                                assertThat(actualObject).isEqualTo(expectedObject);
                            }
                        } else {
                            Set<String> subProperties = fieldsToSubTypes.get(key);
                            actualSubObject = deserialize(serialize(actualObject), Map.class);
                            expectedSubObject = deserialize(serialize(expectedObject), Map.class);
                            compareMapsWithoutFields(actualSubObject, expectedSubObject, subProperties);
                        }
                    }
            );
        }
    }

    public static String getResources(String path) throws IOException {
        return IOUtils.toString(
                Main.class.getResourceAsStream(path),
                Charset.defaultCharset()
        );
    }

    public static String serialize(Object json){
        Gson mapper = new GsonBuilder().create();
        String result = mapper.toJson(json);
        return result;
    }

    public static <T> T deserialize(String json, Type type){
        Gson mapper = new GsonBuilder().create();
        return mapper.fromJson(json, type);
    }

    public static void main(String[] args) throws IOException {
        Map<String, Object> one = deserialize(
                getResources("one.json"), Map.class
        );
        Map<String, Object> two = deserialize(
                getResources("two.json"), Map.class
        );

        compareMapsWithoutGivenFields(one, two, "two.third.number_1");
    }
}
